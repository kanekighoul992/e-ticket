<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePetugasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('petugas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('role_id')->unsigned();
            $table->string('petugas_nip');
            $table->string('petugas_nama');
            $table->string('petugas_jabatan');
            $table->string('petugas_alamat');
            $table->string('petugas_tanggal_lahir');
            $table->string('petugas_foto');
            $table->string('petugas_username');
            $table->string('petugas_password');

            $table->foreign('role_id')->references('id')->on('role');

            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('petugas');
    }
}
